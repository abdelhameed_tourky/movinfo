//
//  MockMovieProvider.swift
//  Movinfo
//
//  Created by Abdelhameed Tourky on 09/04/2017.
//  Copyright © 2017 AMTourky. All rights reserved.
//

import Foundation

protocol MockMovieProvider: MockMovieIDProvider {
    
    var mockMovieTitle: String { get }
    var mockMovieType: String { get }
    var mockMovieYear: String { get }
    var mockMovieimdbID: String { get }
    var mockMoviePosterURL: String { get }
    var mockMovieJSON: [String: Any] { get }

}

extension MockMovieProvider {
    
    var mockMovieTitle: String {
        return "The Fast and the Furious"
    }
    
    var mockMovieType: String {
        return "movie"
    }
    
    var mockMovieYear: String {
        return "2001"
    }
    
    var mockMoviePosterURL: String {
        return "https://images-na.ssl-images-amazon.com/images/M/MV5BNzlkNzVjMDMtOTdhZC00MGE1LTkxODctMzFmMjkwZmMxZjFhXkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_SX300.jpg"
    }
    
    var mockMovieJSON: [String: Any] {
        return [
            "imdbID" : mockMovieimdbID,
            "Year" : mockMovieYear,
            "Type" : mockMovieType,
            "Title" : mockMovieTitle,
            "Poster" : mockMoviePosterURL
        ]
    }
    
    var mockInvalidMovieJSON: [String: Any] {
        return [
            "imdbID" : mockMovieimdbID,
            "Year" : mockMovieYear,
            "Type" : mockMovieType,
            "Poster" : mockMoviePosterURL
        ]
    }

}
