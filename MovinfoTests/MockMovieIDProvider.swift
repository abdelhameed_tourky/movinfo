//
//  MockMovieIDProvider.swift
//  Movinfo
//
//  Created by Abdelhameed Tourky on 09/04/2017.
//  Copyright © 2017 AMTourky. All rights reserved.
//

import Foundation

protocol MockMovieIDProvider {
    
    var mockMovieimdbID: String { get }
    
}

extension MockMovieIDProvider {
    
    var mockMovieimdbID: String {
        return "tt0232500"
    }
    
}
