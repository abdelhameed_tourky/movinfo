//
//  MovieNAPosterTest.swift
//  Movinfo
//
//  Created by Abdelhameed Tourky on 09/04/2017.
//  Copyright © 2017 AMTourky. All rights reserved.
//

import XCTest
import CoreData

@testable import Movinfo

class MovieNAPosterTest: CoreDataTestCase {
    
    var mockNAPosterMovie: Movie {
        let movie = NSEntityDescription.insertNewObject(forEntityName: "Movie", into: self.managedObjectContext) as! Movie
        movie.posterURL = "N/A"
        return movie
    }
    
    func testConvertNAPosterToNil() {
        XCTAssertNil(mockNAPosterMovie.posterURL)
    }
}
