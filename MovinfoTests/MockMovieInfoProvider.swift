//
//  MockMovieInfoProvider.swift
//  Movinfo
//
//  Created by Abdelhameed Tourky on 09/04/2017.
//  Copyright © 2017 AMTourky. All rights reserved.
//

import Foundation

protocol MockMovieInfoProvider: MockMovieIDProvider {
    
    var mockMovieDirector: String { get }
    var mockMovieimdbRating: String { get }
    var mockMovieRating: String { get }
    var mockMovieimdbID: String { get }
    
}

extension MockMovieInfoProvider {
    
    var mockMovieDirector: String {
        return "Rob Cohen"
    }
    
    var mockMovieimdbRating: String {
        return "6.7"
    }
    
    var mockMovieRating: String {
        return "PG-13"
    }
    
    var mockMovieimdbID: String {
        return "tt0232500"
    }
    
    var mockMovieInfoJSON: [String: Any] {
        return [
            "imdbID": mockMovieimdbID,
            "Director" : mockMovieDirector,
            "imdbRating": mockMovieimdbRating,
            "Rated": mockMovieimdbRating
        ]
    }
    
    var mockInvalidMovieInfoJSON: [String: Any] {
        return [
            "Director" : mockMovieDirector,
            "imdbRating": mockMovieimdbRating,
            "Rated": mockMovieimdbRating
        ]
    }
}
