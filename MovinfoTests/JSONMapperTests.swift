//
//  MovinfoTests.swift
//  MovinfoTests
//
//  Created by Abdelhameed Tourky on 09/04/2017.
//  Copyright © 2017 AMTourky. All rights reserved.
//

import XCTest
import CoreData
import SwiftyJSON

@testable import Movinfo

class JSONMapperTests: CoreDataTestCase, MockMovieProvider, MockMovieInfoProvider {
    
    var movieMapper: JSONMapper {
        return MovieMapper(managedObjectContext: managedObjectContext)
    }
    
    var movieInfoMapper: JSONMapper {
        return MovieInfoMapper(managedObjectContext: managedObjectContext)
    }
    
    func testMovieMapper() {
        let movieMapper = MovieMapper(managedObjectContext: managedObjectContext)
        let fastAndFuriousMovie = movieMapper.mapJSONToModel(JSON(mockMovieJSON)) as! Movie
        
        XCTAssert(fastAndFuriousMovie.title == mockMovieTitle)
        XCTAssert(fastAndFuriousMovie.type == mockMovieType)
        XCTAssert(fastAndFuriousMovie.year == mockMovieYear)
        XCTAssert(fastAndFuriousMovie.imdbID == mockMovieimdbID)
        XCTAssert(fastAndFuriousMovie.posterURL == mockMoviePosterURL)
    }
    
    func testInvalidMovieMapper() {
        let invalidMovie = movieMapper.mapJSONToModel(JSON(mockInvalidMovieJSON))
        XCTAssertNil(invalidMovie)
    }
    
    func testMovieInfoMapper() {
        let fastAndFuriousMovieInfo = movieInfoMapper.mapJSONToModel(JSON(mockMovieInfoJSON)) as! MovieInfo
        
        XCTAssert(fastAndFuriousMovieInfo.director == mockMovieDirector)
        XCTAssert(fastAndFuriousMovieInfo.rated == mockMovieimdbRating)
        XCTAssert(fastAndFuriousMovieInfo.imdbRating?.floatValue == NSNumber(value: 6.7).floatValue)
    }
    
    func testInvalidMovieInfoMapper() {
        let invalidMovieInfo = movieInfoMapper.mapJSONToModel(JSON(mockInvalidMovieInfoJSON))
        XCTAssertNil(invalidMovieInfo)
    }
}
