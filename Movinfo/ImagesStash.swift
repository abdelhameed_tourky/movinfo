//
//  ImagesStash.swift
//  Movinfo
//
//  Created by AMTourky on 5/7/16.
//  Copyright © 2016 AMTourky. All rights reserved.
//

import UIKit
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class ImagesStash: NSObject, URLSessionDelegate
{
    
    static let sharedStash = { () -> ImagesStash in 
        let stash = ImagesStash()
        stash.createStashDirectory()
        stash.maintainStashCapacity()
        return stash
    }()
    
    internal typealias ImageVoid = (UIImage)->()
    
    var imageCompletionBlocksMap = [String: [ImageVoid]]()
    var downloadSession: Foundation.URLSession?
    var suggestedStashCapacity = 1000
    var stashDirectoryURL: URL
    {
        let cacheDirURL = FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask)[0]
        return URL(string: cacheDirURL.absoluteString+"ImagesStash")!
    }
    
    
    func imageWithURL(_ url: URL, completion: @escaping (UIImage) -> Void)
    {
        if self.downloadSession == nil
        {
            self.downloadSession = Foundation.URLSession(configuration: URLSessionConfiguration.default, delegate: ImagesStash.sharedStash, delegateQueue: nil)
        }
        
        let imageName = url.lastPathComponent
        
        if let imagePath = self.imagePathForName(imageName), let imageURL = URL(string: imagePath)
        {
            self.addPendingCompletion(completion, toImageName: imageName)
            self.didDownloadImageAtURL(imageURL)
        }
        else
        {
            if !self.completionsPendingOnImageName(imageName)
            {
                self.downloadSession?.downloadTask(with: url).resume()
            }
            self.addPendingCompletion(completion, toImageName: imageName)
        }
    }
    
    func addPendingCompletion(_ completion: @escaping ImageVoid, toImageName imageName: String)
    {
        if self.imageCompletionBlocksMap[imageName] == nil
        {
            self.imageCompletionBlocksMap[imageName] = [ImageVoid]()
        }
        self.imageCompletionBlocksMap[imageName]?.append(completion)
    }
    
    func imagePathForName(_ imageName: String) -> String?
    {
        let imageURL = self.stashDirectoryURL.appendingPathComponent(imageName)
        if FileManager.default.fileExists(atPath: imageURL.path)
        {
            return imageURL.path
        }
        else
        {
            return nil
        }
    }
    
    func URLSession(_ session: Foundation.URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingToURL location: URL)
    {
        guard let imageName = downloadTask.currentRequest?.url?.lastPathComponent
        else {return}
        
        let imageURL = self.stashDirectoryURL.appendingPathComponent(imageName)
        do
        {
            if FileManager.default.fileExists(atPath: imageURL.path)
            {
                try FileManager.default.removeItem(at: imageURL)
            }
            try FileManager.default.moveItem(at: location, to: imageURL)
            self.didDownloadImageAtURL(imageURL)
            
        }
        catch let error as NSError
        {
            print(error.localizedDescription)
        }
    }
    
    func didDownloadImageAtURL(_ imageURL: URL)
    {
        let imageName = imageURL.lastPathComponent
        if self.completionsPendingOnImageName(imageName) {
            if let image = UIImage(contentsOfFile: imageURL.path) {
                self.respondToCompletionsOfImageName(imageName, withImage: image)
            }
        }
    }
    
    func completionsPendingOnImageName(_ imageName: String) -> Bool
    {
        return self.imageCompletionBlocksMap[imageName]?.count > 0
    }
    
    func respondToCompletionsOfImageName(_ imageName: String, withImage image: UIImage)
    {
        if let completions = self.imageCompletionBlocksMap[imageName]
        {
            DispatchQueue.main.async
            {
                for completion in completions
                {
                    completion(image)
                }
            }
            self.imageCompletionBlocksMap[imageName]?.removeAll()
        }
    }
    
    func createStashDirectory()
    {
        do
        {
            if !FileManager.default.fileExists(atPath: self.stashDirectoryURL.path)
            {
                try FileManager.default.createDirectory(at: self.stashDirectoryURL, withIntermediateDirectories: false, attributes:
                    nil)
            }
        }
        catch let error as NSError
        {
            print(error.localizedDescription)
        }
    }
    
    func maintainStashCapacity()
    {
        do
        {
            let fileManager = FileManager.default
            let filePaths = try fileManager.contentsOfDirectory(atPath: self.stashDirectoryURL.path)
            if filePaths.count > self.suggestedStashCapacity
            {
                let fileNameCreationDateMap = self.fileNameCreationDateMap(filePaths)
                let sortedByDatePathAndDateArr = fileNameCreationDateMap.sorted(by: { (fileAndDate1, fileAndDate2) -> Bool in
                    return fileAndDate1.1.compare(fileAndDate2.1) == ComparisonResult.orderedDescending
                })
                let oldFiles = sortedByDatePathAndDateArr[self.suggestedStashCapacity-1..<filePaths.count-1]
                for oldFile in oldFiles
                {
                    let filePath = self.stashDirectoryURL.absoluteString+"/"+oldFile.0
                    try fileManager.removeItem(at: URL(string: filePath)!)
                }
            }
        }
        catch let error as NSError
        {
            print(error.localizedDescription)
        }
        
    }
    
    func fileNameCreationDateMap(_ fileNames: [String]) -> [String: Date]
    {
        var fileNameCreationDateMap = [String: Date]()
        do
        {
            for fileName in fileNames
            {
                let theFilePath = self.stashDirectoryURL.path+"/"+fileName
                let fileAttributes = try FileManager.default.attributesOfItem(atPath: theFilePath)
                if let creationDate = fileAttributes[FileAttributeKey.creationDate] as? Date
                {
                    fileNameCreationDateMap[fileName] = creationDate
                }
            }
        }
        catch let error as NSError
        {
            print(error.localizedDescription)
            return fileNameCreationDateMap
        }
        return fileNameCreationDateMap
    }
    
}
