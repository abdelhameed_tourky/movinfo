//
//  UIImageView+ImageLoader.swift
//  Movinfo
//
//  Created by AMTourky on 5/7/16.
//  Copyright © 2016 AMTourky. All rights reserved.
//

import Foundation
import UIKit

extension UIImageView
{
    func loadImageFromURL(_ url: URL)
    {
        ImagesStash.sharedStash.imageWithURL(url) { (image) in
            UIView.transition(with: self, duration: 0.2, options: .transitionFlipFromTop, animations: {
                self.image = image
                }, completion: nil)
        }
    }
}
