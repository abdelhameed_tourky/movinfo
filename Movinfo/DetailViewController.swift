//
//  DetailViewController.swift
//  Movinfo
//
//  Created by AMTourky on 5/7/16.
//  Copyright © 2016 AMTourky. All rights reserved.
//

import UIKit
import CoreData

class DetailViewController: UITableViewController
{
    @IBOutlet var posterImageView: UIImageView?
    @IBOutlet var titleLabel: UILabel?
    @IBOutlet var plotLabel: UILabel?
    @IBOutlet var actorsLabel: UILabel?
    @IBOutlet var directorLabel: UILabel?
    @IBOutlet var ratedLabel: UILabel?
    @IBOutlet var ratingLabel: UILabel?
    @IBOutlet var activityIndicator: UIActivityIndicatorView?
    var managedObjectContext: NSManagedObjectContext? = nil
    var moviesInformer: MovieInformer? = nil
    var isLoadingMovieInfo = false
    {
        didSet
        {
            if self.isLoadingMovieInfo
            {
                self.activityIndicator?.startAnimating()
            }
            else
            {
                self.activityIndicator?.stopAnimating()
            }
        }
    }
    
    var movie: Movie?
    {
        didSet
        {
            self.configureMovieViews()
            self.configureMovieInfoViews()
        }
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.moviesInformer = MovieInformer(managedObjectContext: self.managedObjectContext!)
        
        self.configureMovieViews()
        self.configureMovieInfoViews()
        
        if self.movie?.info == nil
        {
            self.loadMovieInfo()
        }
    }
    
    func configureMovieViews()
    {
        if let theMovie = self.movie
        {
            let apiKey = (UIApplication.shared.delegate as! AppDelegate).apiKey
            self.titleLabel?.text = theMovie.title
            if let posterURLString = theMovie.posterURL, let imageURL = URL(string: posterURLString+"?apikey=\(apiKey)")
            {
                self.posterImageView?.loadImageFromURL(imageURL)
            }
        }
    }
    
    func configureMovieInfoViews()
    {
        if let theMovieInfo = self.movie?.info
        {
            self.plotLabel?.text = theMovieInfo.plot
            self.actorsLabel?.text = theMovieInfo.actors
            self.directorLabel?.text = theMovieInfo.director
            self.ratedLabel?.text = theMovieInfo.rated
            self.ratingLabel?.text = theMovieInfo.imdbRating?.stringValue
        }
    }
    
    func loadMovieInfo()
    {
        if let theMovieID = self.movie?.imdbID
        {
            self.isLoadingMovieInfo = true
            self.moviesInformer?.getInfoForMovieID(theMovieID, completion: { (info) in
                if let theMovieInfo = info, self.movie?.managedObjectContext != nil
                {
                    self.isLoadingMovieInfo = false
                    self.movie?.info = theMovieInfo
                    self.configureMovieInfoViews()
                }
            })
        }
    }
}
