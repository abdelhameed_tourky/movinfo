//
//  MovieInformer.swift
//  Movinfo
//
//  Created by AMTourky on 5/7/16.
//  Copyright © 2016 AMTourky. All rights reserved.
//

import UIKit
import CoreData
import SwiftyJSON

class MovieInformer: APIConnector
{
    var apiInfoURL = "https://www.omdbapi.com/?"
    var gotInfoHandler: ((_ info: MovieInfo?) -> Void)?
    
    init(managedObjectContext: NSManagedObjectContext)
    {
        let movieInfoMapper = MovieInfoMapper(managedObjectContext: managedObjectContext)
        super.init(managedObjectContext: managedObjectContext, jsonMapper: movieInfoMapper)
    }
    
    func getInfoForMovieID(_ movieID: String, completion:@escaping (_ info: MovieInfo?) -> Void)
    {
        let movieInfoURL = URL(string: "\(self.apiInfoURL)i=\(movieID)")
        self.gotInfoHandler = completion
        self.getRequestToURL(movieInfoURL!)
    }
    
    override func gotJSONResponse(_ json: JSON, fromURL url: URL)
    {
        if json["Response"].string == "True"
        {
            let movieInfo = self.mapper.mapJSONToModel(json)
            if let theGotInfoHandler = self.gotInfoHandler
            {
                DispatchQueue.main.async
                {
                    theGotInfoHandler(movieInfo as? MovieInfo)
                }
            }
        }
    }
}
