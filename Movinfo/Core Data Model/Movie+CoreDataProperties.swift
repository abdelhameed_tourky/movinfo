//
//  Movie+CoreDataProperties.swift
//  Movinfo
//
//  Created by AMTourky on 5/3/16.
//  Copyright © 2016 AMTourky. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Movie {

    @NSManaged var title: String?
    @NSManaged var year: String?
    @NSManaged var imdbID: String?
    @NSManaged var type: String?
    @NSManaged var posterURL: String?
    @NSManaged var info: MovieInfo?
    @NSManaged var recordCreationDate: Date?

}
