//
//  MovieInfo+CoreDataProperties.swift
//  Movinfo
//
//  Created by AMTourky on 5/3/16.
//  Copyright © 2016 AMTourky. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension MovieInfo {

    @NSManaged var releaseDate: Date?
    @NSManaged var runtime: NSNumber?
    @NSManaged var genre: String?
    @NSManaged var director: String?
    @NSManaged var writer: String?
    @NSManaged var actors: String?
    @NSManaged var plot: String?
    @NSManaged var language: String?
    @NSManaged var country: String?
    @NSManaged var imdbRating: NSNumber?
    @NSManaged var rated: String?
    @NSManaged var movie: NSManagedObject?

}
