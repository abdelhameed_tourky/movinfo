//
//  Movie.swift
//  Movinfo
//
//  Created by AMTourky on 5/3/16.
//  Copyright © 2016 AMTourky. All rights reserved.
//

import Foundation
import CoreData


class Movie: NSManagedObject
{
    override func didChangeValue(forKey key: String)
    {
        if key == "posterURL"
        {
            if self.posterURL == "N/A"
            {
                self.posterURL = nil
            }
        }
    }
    
    override func awakeFromInsert()
    {
        self.recordCreationDate = Date()
    }
}
