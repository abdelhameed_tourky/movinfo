//
//  MoviesFinder.swift
//  Movinfo
//
//  Created by AMTourky on 5/3/16.
//  Copyright © 2016 AMTourky. All rights reserved.
//

import UIKit
import CoreData
import SwiftyJSON

class MoviesFinder: APIConnector
{
    var movieName: String?
    var apiSearchURL = "https://www.omdbapi.com/?type=movie"
    var currentResultsPage = 0
    var hasMoreResults = false
    var pendingURL: URL?
    
    init(managedObjectContext: NSManagedObjectContext)
    {
        let movieMapper = MovieMapper(managedObjectContext: managedObjectContext)
        super.init(managedObjectContext: managedObjectContext, jsonMapper: movieMapper)
    }
    
    func searchForMoviesByName(_ name: String?, completion: @escaping (Void) -> Void)
    {
        if let theMovieName = name
        {
            self.movieName = theMovieName
            self.currentResultsPage = 0
            self.findMovies(completion)
        }
        else
        {
            completion()
        }
    }
    
    func findMovies(_ completion: @escaping (Void) -> Void)
    {
        self.currentResultsPage += 1
        let urlCharSet = CharacterSet.urlQueryAllowed
        let movieNameString = self.movieName!.addingPercentEncoding(withAllowedCharacters: urlCharSet)
        self.pendingURL = URL(string: "\(self.apiSearchURL)&s=\(movieNameString!)&page=\(self.currentResultsPage)")
    
        self.getRequestToURL(self.pendingURL!, completion: completion)
    }
    
    func loadMoreMovies(_ completion: @escaping (Void) -> Void)
    {
        if self.hasMoreResults
        {
            self.findMovies(completion)
        }
        else
        {
            completion()
        }
    }
    
    override func gotJSONResponse(_ json: JSON, fromURL url: URL)
    {
        guard let thePendingURL = self.pendingURL, thePendingURL.absoluteString == url.absoluteString
        else { return } // ignore the response if it belongs to a different request than the expected
        
        super.gotJSONResponse(json, fromURL: url)
        
        if json["Response"].string == "True"
        {
            if let searchResult = json["Search"].array
            {
                if let totalResultsString = json["totalResults"].string, let totalResults = Int(totalResultsString)
                {
                    self.checkMoreResultsAvailabilityForTotalResults(totalResults)
                }
                self.createMoviesWithSearchResult(searchResult)
            }
        }
        else if let _ = json["Error"].string
        {
            self.deleteAllMovies()
        }
    }
    
    func checkMoreResultsAvailabilityForTotalResults(_ totalResults: Int)
    {
        if self.currentResultsPage > 0 && totalResults > self.currentResultsPage*10
        {
            self.hasMoreResults = true
        }
        else
        {
            self.hasMoreResults = false
        }
    }
    
    func createMoviesWithSearchResult(_ searchResult: [JSON])
    {
        if self.currentResultsPage == 1
        {
            self.deleteAllMovies()
        }
        for movie in searchResult
        {
            if self.mapper.mapJSONToModel(movie) as? Movie == nil
            {
                print("\n\n\t\tFailed to Map JSON to Movie\n\n")
            }
        }
    }
    
    func deleteAllMovies()
    {
        let fetchMovies = NSFetchRequest<NSFetchRequestResult>(entityName: "Movie")
        fetchMovies.includesPropertyValues = false
        if let allObjects = try? self.managedObjectContext.fetch(fetchMovies)
        {
            if let allMovies = allObjects as? [Movie]
            {
                for movie in allMovies
                {
                    self.managedObjectContext.delete(movie)
                }
                do
                {
                    try self.managedObjectContext.save()
                }
                catch let error as NSError
                {
                    print(error)
                }
            }
        }
    }
    
}
