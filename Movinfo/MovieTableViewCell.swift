//
//  MovieTableViewCell.swift
//  Movinfo
//
//  Created by AMTourky on 5/7/16.
//  Copyright © 2016 AMTourky. All rights reserved.
//

import UIKit

class MovieTableViewCell: UITableViewCell
{
    @IBOutlet var movieImageView: UIImageView?
    @IBOutlet var titleLabel: UILabel?
    
    var movie: Movie?
    {
        didSet
        {
            let apiKey = (UIApplication.shared.delegate as! AppDelegate).apiKey
            self.titleLabel?.text = self.movie?.title
            self.movieImageView?.image = UIImage(named: "movie_poster_placeholder")
            if let posterURLString = self.movie?.posterURL, let imageURL = URL(string: posterURLString+"?apikey=\(apiKey)")
            {
                self.movieImageView?.loadImageFromURL(imageURL)
            }
            
        }
    }
}
