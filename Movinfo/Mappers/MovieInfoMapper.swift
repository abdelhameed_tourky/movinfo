//
//  MovieInfoMapper.swift
//  Movinfo
//
//  Created by AMTourky on 5/7/16.
//  Copyright © 2016 AMTourky. All rights reserved.
//

import UIKit
import CoreData
import SwiftyJSON

class MovieInfoMapper: JSONMapper
{
    override func mapJSONToModel(_ json: JSON) -> NSManagedObject?
    {
        if let _ = json["imdbID"].string
        {
            let movieInfo = NSEntityDescription.insertNewObject(forEntityName: "MovieInfo", into: self.managedObjectContext) as! MovieInfo
            if let theRunTimeString = json["Runtime"].string, let theRunTimeInt = Int(theRunTimeString)
            {
                movieInfo.runtime = NSNumber(value: theRunTimeInt)
            }
            movieInfo.rated = json["Rated"].string
            movieInfo.genre = json["Genre"].string
            movieInfo.director = json["Director"].string
            movieInfo.writer = json["Writer"].string
            movieInfo.actors = json["Actors"].string
            movieInfo.plot = json["Plot"].string
            movieInfo.language = json["Language"].string
            movieInfo.country = json["Country"].string
            if let theRatingString = json["imdbRating"].string, let theRatingFloat = Float(theRatingString)
            {
                movieInfo.imdbRating = NSNumber(value: theRatingFloat)
            }
            return movieInfo
        }
        else
        {
            return super.mapJSONToModel(json)
        }
    }
}
