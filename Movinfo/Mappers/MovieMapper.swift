//
//  MovieMapper.swift
//  Movinfo
//
//  Created by AMTourky on 5/7/16.
//  Copyright © 2016 AMTourky. All rights reserved.
//

import CoreData
import SwiftyJSON

class MovieMapper: JSONMapper
{
    override func mapJSONToModel(_ json: JSON) -> NSManagedObject?
    {
        if let theMovieTitle = json["Title"].string
        {
            let movie = NSEntityDescription.insertNewObject(forEntityName: "Movie", into: self.managedObjectContext) as! Movie
            movie.title = theMovieTitle
            movie.year = json["Year"].string
            movie.imdbID = json["imdbID"].string
            movie.type = json["Type"].string
            movie.posterURL = json["Poster"].string
            return movie
        }
        else
        {
            return super.mapJSONToModel(json)
        }
    }
}
