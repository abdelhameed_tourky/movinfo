//
//  BaseJSONMapper.swift
//  Movinfo
//
//  Created by AMTourky on 5/7/16.
//  Copyright © 2016 AMTourky. All rights reserved.
//

import CoreData
import SwiftyJSON

class JSONMapper: NSObject
{
    var managedObjectContext: NSManagedObjectContext
    
    init(managedObjectContext: NSManagedObjectContext)
    {
        self.managedObjectContext = managedObjectContext
        super.init()
    }
    
    func mapJSONToModel(_ json: JSON) -> NSManagedObject?
    {
        return nil
    }
}
