//
//  MasterViewController.swift
//  Movinfo
//
//  Created by AMTourky on 5/3/16.
//  Copyright © 2016 AMTourky. All rights reserved.
//

import UIKit
import CoreData

class MasterViewController: UITableViewController, NSFetchedResultsControllerDelegate, UISearchBarDelegate {

    static var movieNameDefaultsKey = "MOVIE_NAME"
    @IBOutlet var searchBar: UISearchBar?
    var detailContainerViewController: DetailContainerViewController? = nil
    var managedObjectContext: NSManagedObjectContext? = nil
    var moviesResultsController: NSFetchedResultsController<Movie>? = nil
    var moviesFinder: MoviesFinder? = nil
    var movieName: String?
    {
        didSet
        {
            self.searchForCurrentMovieName()
        }
    }
    
    var isSearchingForMovies = false
    {
        didSet
        {
            if self.isSearchingForMovies
            {
                self.refreshControl?.beginRefreshing()
                if let refreshControlHeight = self.refreshControl?.frame.size.height
                {
                    self.tableView.setContentOffset(CGPoint(x: 0, y: -refreshControlHeight), animated: true)
                }
            }
            else
            {
                self.refreshControl?.endRefreshing()
            }
        }
    }
    
    var isLoadingMoreMovies = false
    {
        didSet
        {
            if self.isLoadingMoreMovies
            {
                let spinner = UIActivityIndicatorView(activityIndicatorStyle: .gray)
                self.tableView.tableFooterView = spinner
                spinner.startAnimating()
            }
            else
            {
                if let spinnerView = self.tableView.tableFooterView as? UIActivityIndicatorView
                {
                    spinnerView.stopAnimating()
                    spinnerView.removeFromSuperview()
                }
            }
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar)
    {
        if let theMovieName = searchBar.text, !theMovieName.isEmpty
        {
            self.movieName = theMovieName.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            searchBar.text = self.movieName
            let usedDefs = UserDefaults.standard
            usedDefs.setValue(theMovieName, forKey: MasterViewController.movieNameDefaultsKey)
            usedDefs.synchronize()
        }
        searchBar.resignFirstResponder()
    }
    
    func searchForCurrentMovieName()
    {
        self.searchBar?.text = self.movieName
        self.isLoadingMoreMovies = false
        self.isSearchingForMovies = true
        self.moviesFinder?.searchForMoviesByName(self.movieName, completion: { (Void) -> Void in
            self.isSearchingForMovies = false
        })
    }
    
    func loadMoreMovies()
    {
        self.isLoadingMoreMovies = true
        self.moviesFinder?.loadMoreMovies({ (Void) -> Void in
            self.isLoadingMoreMovies = false
        })
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let split = self.splitViewController {
            let controllers = split.viewControllers
            self.detailContainerViewController = (controllers[controllers.count-1] as! UINavigationController).topViewController as? DetailContainerViewController
        }
        
        if let theManagedObjectContext = self.managedObjectContext
        {
            self.moviesFinder = MoviesFinder(managedObjectContext: theManagedObjectContext)
        }
        
        self.refreshControl = UIRefreshControl()
        
        self.refreshControl?.addTarget(self, action: #selector(MasterViewController.searchForCurrentMovieName), for: UIControlEvents.valueChanged)
        
        self.initializeFetchResultsController()
        
        if let movieName = UserDefaults.standard.value(forKey: MasterViewController.movieNameDefaultsKey) as? String
        {
            self.movieName = movieName
            self.searchBar?.text = movieName
        }
    }
    
    func initializeFetchResultsController()
    {
        let moviesFetchRequest = NSFetchRequest<Movie>(entityName: "Movie")
        moviesFetchRequest.fetchBatchSize = 20
        
        let sortMovieByTitleDescriptor = NSSortDescriptor(key: "recordCreationDate", ascending: true)
        
        moviesFetchRequest.sortDescriptors = [sortMovieByTitleDescriptor]
        
        self.moviesResultsController = NSFetchedResultsController(fetchRequest: moviesFetchRequest, managedObjectContext: self.managedObjectContext!, sectionNameKeyPath: nil, cacheName: "com.amtourky.movinfo")
        self.moviesResultsController?.delegate = self
        do
        {
            try self.moviesResultsController?.performFetch()
        }
        catch let error as NSError
        {
            print(error)
        }
    }

    override func viewWillAppear(_ animated: Bool)
    {
        self.clearsSelectionOnViewWillAppear = self.splitViewController!.isCollapsed
        super.viewWillAppear(animated)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Segues

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetail" {
            if let indexPath = self.tableView.indexPathForSelectedRow {
            let object = self.moviesResultsController!.object(at: indexPath)
                let controller = (segue.destination as! UINavigationController).topViewController as! DetailContainerViewController
                controller.detailItem = object
                controller.managedObjectContext = self.managedObjectContext
                controller.navigationItem.leftBarButtonItem = self.splitViewController?.displayModeButtonItem
                controller.navigationItem.leftItemsSupplementBackButton = true
            }
        }
    }

    // MARK: - Table View

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let sectionInfo = self.moviesResultsController?.sections?[section]
        {
            return sectionInfo.numberOfObjects
        }
        else
        {
            return 0
        }
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> MovieTableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MOVIE_CELL", for: indexPath) as! MovieTableViewCell
        let movieObject = self.moviesResultsController!.object(at: indexPath)
        cell.movie = movieObject
        if let numOfMovies = self.moviesResultsController?.fetchedObjects?.count, numOfMovies-1 == indexPath.row && !self.isLoadingMoreMovies
        {
            self.loadMoreMovies()
        }
        return cell
    }


    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.tableView.beginUpdates()
    }

    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        switch type {
            case .insert:
                tableView.insertRows(at: [newIndexPath!], with: .fade)
            case .delete:
                tableView.deleteRows(at: [indexPath!], with: .fade)
            default: ()
        }
    }

    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.tableView.endUpdates()
    }

}

