//
//  DetailContainerViewController.swift
//  Movinfo
//
//  Created by AMTourky on 5/3/16.
//  Copyright © 2016 AMTourky. All rights reserved.
//

import UIKit
import CoreData

class DetailContainerViewController: UIViewController {
    
    var detailItem: AnyObject?
    var managedObjectContext: NSManagedObjectContext? = nil

    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if let theDetailViewController = segue.destination as? DetailViewController
        {
            theDetailViewController.managedObjectContext = self.managedObjectContext
            theDetailViewController.movie = self.detailItem as? Movie
            self.navigationItem.title = (self.detailItem as? Movie)?.title
        }
    }
}

