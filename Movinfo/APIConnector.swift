//
//  APIConnector.swift
//  Movinfo
//
//  Created by AMTourky on 5/7/16.
//  Copyright © 2016 AMTourky. All rights reserved.
//

import UIKit
import CoreData
import SwiftyJSON

class APIConnector: NSObject
{
    var managedObjectContext: NSManagedObjectContext
    var mapper: JSONMapper
    var defaultSession: URLSession?
    
    init(managedObjectContext: NSManagedObjectContext, jsonMapper mapper: JSONMapper)
    {
        self.managedObjectContext = managedObjectContext
        self.mapper = mapper
        super.init()
        self.defaultSession = URLSession(configuration: URLSessionConfiguration.default, delegate: nil, delegateQueue: nil)
    }
    
    deinit
    {
        self.defaultSession?.invalidateAndCancel()
    }
    
    func getRequestToURL(_ url: URL, completion: ((Void) -> Void)? = nil)
    {
        self.startAnimatingNetworkActivityIndicator()
        let searchMovieTask = self.defaultSession?.dataTask(with: url, completionHandler: {
            data, response, error in
            if let error = error
            {
                print(error.localizedDescription)
                DispatchQueue.main.async
                {
                    UIAlertView(title: "Ops!", message: "Can't reach the server, try again in a moment.", delegate: nil, cancelButtonTitle: "OK").show()
                }
            }
            else if let theData = data, let theURLResponse = response as? HTTPURLResponse, let url = theURLResponse.url, theURLResponse.statusCode == 200
            {
                let json = JSON(data: theData)
                DispatchQueue.main.async
                {
                    self.gotJSONResponse(json, fromURL: url)
                }
            }
            self.stopAnimatingNetworkActivityIndicator()
            if let theCompletionBlock = completion
            {
                DispatchQueue.main.async
                {
                    theCompletionBlock()
                }
            }
        })
        searchMovieTask?.resume()
    }
    
    func startAnimatingNetworkActivityIndicator()
    {
        DispatchQueue.main.async
        {
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
        }
    }
    
    func stopAnimatingNetworkActivityIndicator()
    {
        DispatchQueue.main.async
        {
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
        }
    }
    
    func gotJSONResponse(_ json: JSON, fromURL url: URL)
    {
        if let error = json["Error"].string, json["Response"].string == "False"
        {
            DispatchQueue.main.async
            {
                UIAlertView(title: "Error", message: error, delegate: nil, cancelButtonTitle: "OK").show()
            }
        }
    }
    
}
