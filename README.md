# README #

Movinfo, an universal iOS app for searching movies and getting info from: https://omdbapi.com/

Minimum iOS version: iOS 8.
Built with XCode version: 7.3. UPDATED with XCode 8.1 to Swift 3
Using one external library (Swifty JSON) using cocoa pods: https://github.com/SwiftyJSON/SwiftyJSON.

### Features ###

-Search using movie title.
-Show movie poster, plot, rating, actors..
-Cache images, with animation on load.
-Cache search results (movies and movies' info).
-Paginate search results.